@extends('layouts.app')

@section('content')
    <h1 class="mb-2">Heriberto's Stores</h1>
    <div class="card">
        <div class="card-body">
            <div class="clearfix">
                <ul class="list-inline float-right p-0 m-0">
                    <li class="list-inline-item">
                        <a href="#" class"text-muted" data-map-toggle="#storesMap">Map</a> | 
                        <a href="#" class"text-muted" data-map-toggle="#storesList">List</a>
                    </li>
                </ul>
            </div>
            <hr class="clearfix my-2">
            <div id="storesList" style="display: none;">
                <div class="card">
                    <ul class="list-group list-group-flush">
                    @foreach($stores as $key => $store)
                        <li class="list-group-item">
                            <div class="card-title mb-1"><a href="{{ url('/stores/' . $store->id) }}">{{ $store->name }}</a></div>
                            <div class="card-text mb-3">
                                <address class="m-0">
                                    {{ $store->street_address }} <br>
                                    {{ $store->city . ', ' . $store->state . ' ' . $store->zip_code }}
                                </address>
                            </div>
                            <p class="card-text">
                                <a href="{{url('/stores/' . $store->id)}}" class="text-muted"><small>View details</small></a>
                            </p>
                        </li>
                    @endforeach
                    </ul><!-- /.list-group -->
                </div><!-- /.card -->
            </div>
            <div id="storesMap">
                {!! Mapper::render() !!}
            </div>
        </div>
    </div>
@endsection