<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Stores</title>
    <link rel="stylesheet" href="{{ url('/app.css') }}">
</head>
<body>
    <div id="app">
        <main class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="mb-2">Stores</h1>
                </div>
                <div class="col-12 card">
                    <div class="card-body">
                        <ul class="inline-list float-right p-0 m-0">
                            <li class="inline-list-item">
                                <a href="#" data-toggle="#storesMap">Map</a>
                            </li>
                            <li class="inline-list-item">
                                <a href="#" data-toggle="#storesList">List</a>
                            </li>
                        </ul>
                        <hr class="w-100 my-2">
                        <div id="storesList">
                            
                        </div>
                        <div id="storesMap">
                            {{-- {!! Mapper::render() !!} --}}
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script src="{{ url('/app.js') }}"></script>
</body>
</html>