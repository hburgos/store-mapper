<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Store;
use Mapper;

class StoreController extends Controller
{
    /**
     * Show all the stores
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = Store::all();

        foreach($stores as $key => $store)
        {
            $address = $store->street_address . ' ' . $store->city . ', ' . $store->state . ' ' . $store->zip_code;
            $loc = Mapper::location($address);

            if($key == 0) {
                Mapper::map($loc->getLatitude(), $loc->getLongitude(), ['zoom' => 13, 'center' => false, 'marker' => false, 
                    'type' => 'ROADMAP', 'fullscreenControl' => false, 'streetViewControl' => false, 'mapTypeControl' => false]);
            }
            Mapper::informationWindow($loc->getLatitude(), $loc->getLongitude(), ($store->name . '<br>' . $address . '<br>' . $store->telephone), 
                ['open' => false, 'maxWidth'=> 250]);
        }

        return view('store.index', compact('stores'));
    }

    /**
     * Show the given store
     *
     * @param Store $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        Mapper::location($store->street_address . ' ' . $store->city . ', ' . $store->state . ' ' . $store->zip_code)
            ->map(['zoom' => 13, 'center' => true, 'marker' => true, 'type' => 'ROADMAP', 'fullscreenControl' => false, 
            'streetViewControl' => false, 'mapTypeControl' => true]);
        
        return view('store.show', compact('store'));
    }
}
