<?php

use Faker\Generator as Faker;

$factory->define(App\Store::class, function (Faker $faker) {

    return [
        'name' => $faker->company,
        'street_address' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'zip_code' => $faker->postcode,
        'telephone' => $faker->phoneNumber,
    ];
});
