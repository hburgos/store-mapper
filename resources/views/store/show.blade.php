@extends('layouts.app')

@section('content')
    <p><a href="{{route('stores')}}" class="text-dark">Go back</a></p>
    <div class="card">
        <div class="card-body">
            <div class="card-title border-bottom mb-3">
                <h1 class="mb-0">Store: {{$store->name}}</h1>
            </div>
            <div class="card-text mb-3">
                <address class="m-0">
                    {{ $store->street_address }} <br>
                    {{ $store->city . ', ' . $store->state . ' ' . $store->zip_code }}
                </address>
                Phone: {{$store->telephone}}
            </div>
            <div class="card-text">
                <h4>Store Map:</h4>
                <div class="map-wrapper">
                    {!! Mapper::render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection