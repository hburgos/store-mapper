<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Heriberto's Stores</title>
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">
    @yield('styles')
</head>
<body>
    <div id="app" class="my-4">
        <main class="container">
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1">
                    @yield('content')
                </div>
            </div><!-- /.row -->
        </main><!-- /.container -->
    </div>
    <script src="{{ url('/js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>