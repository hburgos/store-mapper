# Store mapper app challenge

Estimated up to 3-4 hours

### Constraints:
1. Laravel 5.6 (php 7.1+)
2. Git

### Store model:
- Name
- Street address
- City
- State
- Zip code
- Telephone

### Requirements:
1. Unit testing - 3 tests minimum
2. Route & View of the stores from the database on a map.

### R&D resources:
Potential libraries from initial R&D. Optionally you can use one of these or find/use your own solution.

1. [farhanwazir/laravelgooglemaps](https://github.com/farhanwazir/laravelgooglemaps)
2. [bradcornford/Googlmapper](https://github.com/bradcornford/Googlmapper)
3. [alexpechkarev/google-maps](https://github.com/alexpechkarev/google-maps)

### Deliverables:
- Email a zip file of your git repo (with versions intact and without vendors)
- Or provide a link to a repo of the project (like GitHub or BitBucket)

## Heriberto's Stores Instructions
----------------------------------

### Features
- All Stores view (Google maps or list)
- Single Store view

### Installation
```sh
$ composer install
```

### Load database configuration
```sh
$ php artisan migrate
```

Optional database seed
```sh
$ php artisan db:seed
```

### Usage
```sh
$ php artisan serve
```
Navigate to [http://localhost:8000](http://localhost:8000)

### Testing
```sh
$ phpunit
```
If it's not installed in the system or alias created, you can use from Laravel's root directory:
```sh
$ vendor/bin/phpunit
```

### Future development
1. Store locations (latitude and longitude) into the database.
2. Store business hours.
3. Store creation view.