/**
 * Toggle between map/list views
 */

jQuery(document).ready(function(){
    // Declaration
    var $app = $("#app"),
        $map_toggle = $('[data-map-toggle]');

    // Binding
    $map_toggle.length && $map_toggle.on('click', function(e){
        e.preventDefault();
        var id = $(this).data('map-toggle');
        $map_toggle.toggleClass('selected');
        $app.find('[id^="stores"]:visible').fadeOut(300, function(){
            $(id).fadeIn(400);
        });
    });
});