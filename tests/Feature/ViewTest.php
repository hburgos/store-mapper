<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Store;

class ViewTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * Stores listing view
     *
     * @return void
     */
    public function stores_listing_view()
    {
        $response = $this->get('/stores');

        $response->assertOk()
            ->assertViewIs('store.index')
            ->assertSeeInOrder([
                'Stores',
                'Map',
                'List',
                'id="storesList"',
                'id="storesMap"',
            ]);
    }

    /**
     * @test
     * Single Store details test
     *
     * @return void
     */
    public function single_store_details_view()
    {
        $stores = factory(Store::class, 3)->create();
        $response = $this->get('/stores/' . $stores->first()->id);

        $response->assertOk()
            ->assertViewIs('store.show')
            ->assertViewHas('store');
    }
}
