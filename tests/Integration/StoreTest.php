<?php

namespace Tests\Integration;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Store;

class StoreTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * @test
     * Creating records on database
     *
     * @return void
     */
    public function store_model_database_create_records()
    {
        $stores = factory(Store::class, 3)->create();
        $this->assertCount(3, $stores);
    }
}
