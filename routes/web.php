<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('stores');
});

Route::get('/stores', 'StoreController@index')->name('stores');
Route::get('/stores/{store}', 'StoreController@show')->name('store.show');
