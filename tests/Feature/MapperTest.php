<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Mapper;

class MapperTest extends TestCase
{

    /**
     * @test
     * Mapper package geolocation test
     *
     * @return void
     */
    public function mapper_geolocation()
    {
        $location = Mapper::location('Sunny Isles Beach, FL');

        $this->assertObjectHasAttribute('latitude', $location);
        $this->assertObjectHasAttribute('longitude', $location);
        
        $this->assertInternalType( 'float', $location->getLatitude() );
        $this->assertInternalType( 'float', $location->getLongitude() );
    }
}
